% Formats structurés et gestion de version
% gau real@lebib.org
% 10.10.2018

# Publier en 2018
- multiplication des supports de l'information
- sortir des silos imposés par des grandes entreprises
- éviter la dette technique et assurer la continuité et la pérénité de nos productions
- accessibilité & universalité de l'information

-----

# différencier le fond et la forme
Pour appliquer **différentes formes** (design) à un contenu, il est nécessaire de **structurer** correctement ce contenu. Pour cela nous utilisons des langages des balisages
- le plus connu : html, 
- le plus complexe : xml,
- le plus simple : markdown
...

-----

## langages de balises
Langages spécialisés dans l'enrichissement d'information textuelle.  
Ils utilisent des **balises**, unités syntaxiques délimitant une _séquence_ de caractères ou marquant une _position_ précise à l'intérieur d'un flux de caractères.  
Chaque élément se trouve entre deux balises, une ouvrante et une fermante.  
Chaque balise est inclue entre les signes inférieur et supérieur :  

	<balise>texte à afficher</balise>

-----

Chaque balise peut contenir des attributs prédéfinis par la norme définissant le langage. 
Les attributs comprennent généralement une valeur.

 	<balise attribut=”valeur”>texte à afficher</balise>

-----

Les espaces et accents sont interdits dans les valeurs.
Tous les contenus non textuels ne sont pas incrustés, une balise spécifique permet de faire une référence via un lien vers les fichiers contenant ces éléments.

	<img src=”images/geneve.jpg”>   

indique de placer à cet endroit l’image geneve.jpg qui se trouve dans le dossier images.

-----

C’est le dispositif de lecture, généralement un navigateur web, qui se charge de regrouper et présenter tous les éléments. 


-----

#### Métadonnées
Ce sont des données qui décrivent les données. Généralement invisibles pour le lecteur, elles permettent de décrire très précisément divers éléments. Elles s'écrivent avec un attribut nom et un attribut contenu.

	<meta name=”language” content”fr”/>

-----

### HTML

-----

#### Paragraphes

	<p>paragraphe</p>
	<h1>Titre niveau 1</h1>
	<h2>Titre niveau 2</h2>
	<h3></h3>  
	<h4></h4>  
	<h5></h5>  
	<h6></h6> 

-----

#### Caractères 

	<em>emphase</em> 
	<i>italique visuelle</i>
	<strong>emphase forte</strong>  
	<b>gras visuel</b>

-----

#### Attributs : Informations additionnelles

	<p class=“exemple”> 
Les classes permettent de définir des variantes de styles au sein d’une même sémantique. Ces éléments ne seront pas pris en compte par la synthèse vocale. On peut les nommer librement, mais il est préférable de leur donner un nom qui ait un sens. Les espaces et les accents sont interdits. 

	<p id=“premier”> 
Identification unique d’un élément permettant de s’y référer ultérieurement. 

	<p lang="en" xml:lang="en"> 
Permet de modifier le langage d’une portion du document.
La liste complète des attributs disponibles est définie par le W3C.


-----

### HTML 5
Introduit de nouvelles balises qui permettent de décrire la structure des documents 
-----

	<header> : le haut de page.  

	<footer> : le bas de page.  

	<section> : regroupe des éléments de thématique identique. Peut avoir son propre header et footer.  

	<article> :  du contenu ayant son propre sens indépendamment du reste des autres éléments de la page, ce contenu est distribuable et réutilisable. Il peut avoir son propre header et footer.  

	<aside> : des éléments qui sont reliés au contenu mais sans en faire partie trouvent place dans ces balises. Dans un livre, cela correspond au texte mis dans les colonnes sur les côtés ou au texte des notes.  

	<nav> : pour la navigation.  

-----

### XHTML
C'est la version XML de HTML.   
Le X indique que l’on utilise la syntaxe XML, plus stricte.   
Le X veut dire aussi eXtensible.  
On peut donc utiliser d’autres technologies basées sur le XML comme le SVG et le MathML.

-----

### CSS

-----

#### Selecteurs
	h1 { } style de balises  

	.exemple { } style de classes  

	Options  
	{  
    font-size: 1em;  
    color: blue;  
    }  

-----

#### Cascade
Il n’est pas nécessaire de définir tous les éléments d’un style. On peut ainsi paramétrer la taille et la typographie d’un titre, et définir les couleurs à part dans des classes dédiées.  

	<h1 class=”blue”>Titre bleu</h1>  
  
	h1 {font-size: 1em;}  
	.blue {color: blue;}  


-----

### markdown
basé sur du texte
lisible
facile a apprendre
facile à convertir
	
	# Titre 1
	## Titre 2
	*emphase*
	**emphase forte**

-----

## transformations
exemple en utilisant pandoc

-----

# gérer les différentes versions d'un document

Tout projet entraîne des

- erreurs, 
- corrections, 
- retours en arriére, 
- réorientations,  
- etc.  

La meilleure façon de l'anticiper est dés l'origine de conserver et organiser différentes versions d'un document.  

Et ça nous aidera pour partager le travail !

-----

GIT : un gestionaire de version distribué

-----

## vocabulaire

Depot = repertoire contenant des fichiers


Commit = changement validé par l'utilisateur

-----

## Exemple
Un nouveau dépot à partir d'un projet existant

	$ git init
	$ git add all
	$ git commit -m"ça pars de là"
	$ git push

-----

Pour vérifier les différentes versions : 

	$ git log
	$ git diff

-----

Travailler sur un dépot existant en ligne

	$ git init
	$ git clone adresse

-----

modifier les fichiers avec votre éditeur de texte préféré puis

	$ git init
	$ git add all
	$ git commit -m"j'ai tout donné"
	$ git push

-----

## Organiser la forêt

Tree / branch

	$ git branch design-engage-revu
	$ git checkout design-engage-revu

élagage régulier

	$ git merge design-engage-revu

-----

## ressources
- https://gitlab.com/users/antoinentl/projects  
- Pro Git, Scott Chacon, Ben Straub, Apress
Version française : https://git-scm.com/book/fr/v2

















